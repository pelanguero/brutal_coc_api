package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
)

//"github.com/gin-gonic/gin"

type User struct {
	//id==tag
	tag         string
	id          string
	Nick        string
	DiscordUser string
	Pass        string
	Rango       string
	Player      Jugador
}
type Jugador struct {
	tag           string
	name          string
	townHallLevel string
	//townHallWeaponLevel int
	expLevel            string
	trofeos             string
	bestTrofeos         string
	warStars            string
	attackWins          string
	defenseWins         string
	builderHallLevel    string
	versusTrofeos       string
	bestVersusTrofeos   string
	versusBatleWins     string
	rol                 string
	donaciones          string
	donacionesRecibidas string
	clantag             string
	clan                Clan
	liga                Liga
	tropas              []Tropa
}

type Tropa struct {
	name     string `json:"name"`
	level    string `json:"level"`
	maxLevel string `json:"maxLevel"`
	aldea    string `json:"village"`
}

type Clan struct {
	tag       string
	name      string
	clanlevel string
	escudo    string
}
type Liga struct {
	nombre string
	imagen string
}

var coc_token string

func main() {
	fmt.Println("Inicio fmt")
	enverr := godotenv.Load()
	if enverr != nil {
		fmt.Println("Error al cargar .env")
	}
	coc_token = os.Getenv("TOKEN-COC")

	router := gin.Default()
	router.Use(CORSMiddleware())
	router.POST("/register", registrarUsuario)

	router.Run(":8080")

}
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control,token, X-Requested-With,access-control-allow-origin")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func registrarUsuario(c *gin.Context) {
	var jug Jugador
	var uuser User
	var result map[string]string
	buff := new(bytes.Buffer)
	buff.ReadFrom(c.Request.Body)
	json.Unmarshal([]byte(buff.String()), &result)
	fmt.Println(result["tag"])
	tag := result["tag"]
	jug = createUserFromApi(tag)
	insertPlayerToDB(jug)
	uuser = createUserFromMap(result, jug)
	insertUserToDB(uuser)
	c.JSON(http.StatusOK, gin.H{"respuesta": 200})
}

/*
usuario,discord,password,tag
*/
func createUserFromMap(uuser map[string]string, plrayer Jugador) User {
	var retorno User
	iidd := xid.New()
	retorno.id = iidd.String()
	retorno.Nick = uuser["usuario"]
	retorno.DiscordUser = uuser["discord"]
	retorno.Pass = hashpw(uuser["password"])
	retorno.Rango = plrayer.rol
	retorno.tag = plrayer.tag

	return retorno
}
func insertUserToDB(uuser User) {
	db, err := sql.Open("mysql", "root:mysql@tcp(127.0.0.1:3306)/backcoctest")
	if err != nil {
		panic(err.Error())
	}
	insert, rerror := db.Query("INSERT INTO usuarios(id,name,discordUser,passw,rango,tag) VALUES " + "(" + "\"" + uuser.id + "\"" + "," + "\"" + uuser.Nick + "\"" + "," + "\"" + uuser.DiscordUser + "\"" + "," + "\"" + uuser.Pass + "\"" + "," + "\"" + uuser.Rango + "\"" + "," + "\"" + uuser.tag + "\"" + ")")
	if rerror != nil {
		panic(rerror)
	}
	defer insert.Close()
	defer db.Close()
}

//es para no guardar el pw plano
func hashpw(pwd string) string {
	bytestr := []byte(pwd)
	hashh, err := bcrypt.GenerateFromPassword(bytestr, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hashh)
}

//verifica el pw con hash y el posible pw plano
func verificarpw(hashedpw string, plain string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedpw), []byte(plain))
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func insertPlayerToDB(plyy Jugador) {
	db, err := sql.Open("mysql", "root:mysql@tcp(127.0.0.1:3306)/backcoctest")
	if err != nil {
		panic(err.Error())
	}
	fmt.Println("INSERT INTO jugadores(tag,name,townHallLevel,expLevel,trofeos,bestTrofeos,warStars,attackWins,defenseWins,builderHallLevel,versusTrofeos,bestVersusTrofeos,versusBatleWins,rol,donaciones,donacionesRecibidas,clantag) VALUES " + "(" + plyy.tag + "," + plyy.name + "," + plyy.townHallLevel + "," + plyy.expLevel + "," + plyy.trofeos + "," + plyy.bestTrofeos + "," + plyy.warStars + "," + plyy.attackWins + "," + plyy.defenseWins + "," + plyy.builderHallLevel + "," + plyy.versusTrofeos + "," + plyy.bestVersusTrofeos + "," + plyy.versusBatleWins + "," + plyy.rol + "," + plyy.donaciones + "," + plyy.donacionesRecibidas + "," + plyy.clantag + ")")
	insert, teerro := db.Query("INSERT INTO jugadores(tag,name,townHallLevel,expLevel,trofeos,bestTrofeos,warStars,attackWins,defenseWins,builderHallLevel,versusTrofeos,bestVersusTrofeos,versusBatleWins,rol,donaciones,donacionesRecibidas,clantag) VALUES " + "(" + "\"" + plyy.tag + "\"" + "," + "\"" + plyy.name + "\"" + "," + "\"" + plyy.townHallLevel + "\"" + "," + "\"" + plyy.expLevel + "\"" + "," + "\"" + plyy.trofeos + "\"" + "," + "\"" + plyy.bestTrofeos + "\"" + "," + "\"" + plyy.warStars + "\"" + "," + "\"" + plyy.attackWins + "\"" + "," + "\"" + plyy.defenseWins + "\"" + "," + "\"" + plyy.builderHallLevel + "\"" + "," + "\"" + plyy.versusTrofeos + "\"" + "," + "\"" + plyy.bestVersusTrofeos + "\"" + "," + "\"" + plyy.versusBatleWins + "\"" + "," + "\"" + plyy.rol + "\"" + "," + "\"" + plyy.donaciones + "\"" + "," + "\"" + plyy.donacionesRecibidas + "\"" + "," + "\"" + plyy.clantag + "\"" + ")")
	if teerro != nil {
		fmt.Println("Dio Error:")
		panic(teerro)
	}
	defer insert.Close()
	defer db.Close()
}

func createUserFromApi(tagg string) Jugador {
	var player Jugador
	var result map[string]interface{}
	client := &http.Client{
		CheckRedirect: nil,
	}
	fmt.Println(tagg)
	req, eror := http.NewRequest("GET", "https://api.clashofclans.com/v1/players/%23"+tagg, nil)
	if eror != nil {
		fmt.Println(eror.Error())
		return player
	}
	req.Header.Add("authorization", " Bearer "+coc_token)
	resp, eror := client.Do(req)
	if eror != nil {
		fmt.Println(eror.Error())
		return player
	}
	buffe := new(bytes.Buffer)
	buffe.ReadFrom(resp.Body)
	json.Unmarshal([]byte(buffe.String()), &result)
	mapToPlayer(&result, &player, buffe.String())

	defer resp.Body.Close()
	return player
}
func mapToPlayer(proe *map[string]interface{}, ply *Jugador, base string) {
	var clan Clan
	var tropas []Tropa
	var liga Liga
	ply.tag = fmt.Sprintf("%s", (*proe)["tag"])
	ply.name = fmt.Sprintf("%s", (*proe)["name"])
	ply.townHallLevel = fmt.Sprintf("%.0f", (*proe)["townHallLevel"])
	ply.expLevel = fmt.Sprintf("%.0f", (*proe)["expLevel"])
	ply.trofeos = fmt.Sprintf("%.0f", (*proe)["trophies"])
	ply.bestTrofeos = fmt.Sprintf("%.0f", (*proe)["bestTrophies"])
	ply.warStars = fmt.Sprintf("%.0f", (*proe)["warStars"])
	ply.attackWins = fmt.Sprintf("%.0f", (*proe)["attackWins"])
	ply.defenseWins = fmt.Sprintf("%.0f", (*proe)["defenseWins"])
	ply.builderHallLevel = fmt.Sprintf("%.0f", (*proe)["builderHallLevel"])
	ply.versusTrofeos = fmt.Sprintf("%.0f", (*proe)["versusTrophies"])
	ply.bestVersusTrofeos = fmt.Sprintf("%.0f", (*proe)["bestVersusTrophies"])
	ply.versusBatleWins = fmt.Sprintf("%.0f", (*proe)["versusBattleWins"])
	ply.rol = fmt.Sprintf("%s", (*proe)["role"])
	ply.donaciones = fmt.Sprintf("%.0f", (*proe)["donations"])
	ply.donacionesRecibidas = fmt.Sprintf("%.0f", (*proe)["donationsReceived"])
	mapToClan((*proe)["clan"].(map[string]interface{}), &clan)
	ply.clan = clan
	ply.clantag = clan.tag
	mapToLiga((*proe)["league"].(map[string]interface{}), &liga)
	ply.liga = liga
	mapToTropas((*proe)["troops"].([]interface{}), &tropas)
	ply.tropas = tropas
}
func mapToClan(clans map[string]interface{}, cclan *Clan) {
	temp := clans["badgeUrls"].(map[string]interface{})
	cclan.tag = fmt.Sprintf("%s", clans["tag"])
	cclan.name = fmt.Sprintf("%s", clans["name"])
	cclan.clanlevel = fmt.Sprintf("%f", clans["clanLevel"])
	cclan.escudo = fmt.Sprintf("%s", temp["large"])
}
func mapToLiga(lliga map[string]interface{}, liiga *Liga) {
	tempp := lliga["iconUrls"].(map[string]interface{})
	liiga.nombre = fmt.Sprintf("%s", lliga["name"])
	liiga.imagen = fmt.Sprintf("%s", tempp["medium"])
}
func mapToTropas(troppas []interface{}, trropa *[]Tropa) {

	for h, j := range troppas {
		var trops Tropa
		mapp := j.(map[string]interface{})
		fmt.Println(h)
		trops.name = fmt.Sprintf("%s", mapp["name"])
		fmt.Println(trops.name)
		trops.level = fmt.Sprintf("%s", mapp["level"])
		trops.maxLevel = fmt.Sprintf("%s", mapp["maxLevel"])
		trops.aldea = fmt.Sprintf("%s", mapp["village"])
	}

}

// func agregarUsuariodb(varUs *User) bool {

// }
